<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Obat;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('user.homeuser');
    }
    public function admin()
    {
        return view('admin.homeadmin');
    }
    public function home()
    {
        return view('home');
    }
    public function obat()
    {
        $obat = DB::table('obat')->get();
        return view('user.showobat', ['obat'=>$obat]);
    }
}
