<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Obat;

class ObatController extends Controller
{
    public function index()
    {
        $obat = \App\Obat::all();
        return view('crud.obat',['obat' => $obat]);
    }
    public function cari(Request $request)
    {
        $cari = $request->cari;
        $obat = DB::table('obat')
        ->where('name','like',"%".$cari."%")
        ->paginate();
        return view('crud.obat',['obat' => $obat]);
    }
    public function create(Request $request)
    {
        $this->validate($request, [
            'name'          => 'required',
            'harga'         => 'required',
            'stok'          => 'required',
            'keterangan'    => 'required',
            'file'          => 'required|file|image|mimes:jpeg,png,jpg|max:2048',
            
        ]);
        
        $file = $request->file('file');
        $nama_file = time()."_".$file->getClientOriginalName();
        $tujuan_upload = 'data_file';
        $file->move($tujuan_upload,$nama_file);       

        Obat::create([
        'name' => $request->name,
        'harga' => $request->harga,
        'stok' => $request->stok,
        'keterangan' => $request->keterangan,
        'file' => $nama_file,
        
        ]);
        return redirect('/obat')->with('sukses','Data Obat Berhasil Ditambahkan');
    }
    public function store(Request $request)
    {
        //    
    }
    public function show()
    {
        $obat = Obat::all();
        return view('user.showobat',compact('obat'));
    }
    public function edit($id)
    {
        $obat = Obat::find($id);
        return view('crud.editobat',['obat'=> $obat]);
    }
    public function update(Request $request, $id)
    {
        $obat = \App\Obat::findOrFail($id);
        $obat->name = $request->input('name');
        $obat->harga = $request->input('harga');
        $obat->stok = $request->input('stok');
        $obat->keterangan = $request->input('keterangan');
        if (empty($request->file('file'))){
            $obat->file = $obat->file;
        }
        else{
            unlink('data_file/'.$obat->file); //menghapus file lama
            $file = $request->file('file');
            $ext = $file->getClientOriginalExtension();
            $newName = rand(100000,1001238912).".".$ext;
            $file->move('data_file/',$newName);
            $obat->file = $newName;
        }
        $obat->save();
        return redirect('/obat')->with('alert-success','Data berhasil diubah!');    
    }
    public function destroy($id)
    {
        DB::table('obat')->where('id',$id)->delete();
        return redirect('/obat');
    }
}