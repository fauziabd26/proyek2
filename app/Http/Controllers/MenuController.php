<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MenuController extends Controller
{
    public function index()
    {
        return view('home');
    }
    public function galeri()
    {
        return view('galeri');
    }
    public function klinik()
    {
        return view('klinik');
    }
    public function informasi()
    {
        return view('informasi');
    }
    public function protokol()
    {
        return view('protokol');
    }
    public function germas()
    {
        return view('germas');
    }
    public function register()
    {
        return view('auths.register');
    }
}
