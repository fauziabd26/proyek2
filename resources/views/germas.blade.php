@extends('template')
@section('tittle','germas')
@section('content')
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <h1> GERAKAN MASYARAKAT 3M</h1>
      <img class="imgl borderedbox inspace-5" src="../images/3m.webp" alt="">
      <p>
      Masker terbukti sebagai salah satu cara yang paling efektif untuk mencegah penularan COVID-19. Menyadari akan hal tersebut, para relawan yang tergabung dalam Gerakan Memakai Masker Gratis (GEMAS) membagikan 50 ribu masker.
      <br> <br> Kali ini GEMAS menyerahkan bantuan tersebut kepada Polri. Adalah Wakapolri, Komjen. Pol. Dr. Drs. Gatot Eddy Pramono, M.Si. yang mewakili penerimaan tersebut.
      <br> <br>
      "Terima kasih untuk rekan-rekan di Polri yang turut berperan aktif dalam penanganan COVID-19 ini. Kami percaya bahwa Covid-19 ini tidak bisa diatasi hanya seorang diri, namun harus saling berkolaborasi antara masyarakat, dan pemerintah, terutama Polri yang turut ikut kelapangan,” kata Wanda Ponika dari GEMAS.
      <br> <br> Gatot pun berterima kasih kepada para relawan. Ia mengatakan, polisi tidak bisa bekerja sendiri untuk menghadapi pandemi. Untuk itu, kegiatan relawan yang positif seperti ini sangat diapresiasi oleh kepolisian.
      <br> <br> "Karena ini merupakan tantangan kita bersama. Kami tidak bisa bekerja sendiri, tapi juga mengajak seluruh masyarakat. Yang terpenting, mari kita fokus memutus mata rantai covid-19 ini bersama," kata Gatot.
      <br> <br>Hal itu selaras dengan pesan #satgascovid19 untuk melakukan 3M: Mencuci tangan #cucitangan, menjaga jarak #jagajarak, dan memakai masker #pakaimasker. Gatot menyuarakan hal yang senada sebagai kunci menghadapi pandemi.
      <br> <br>"Masyarakat harus waspada bahwa virus ini memang benar adanya dan bisa menyerang siapa saja. Untuk itu, mulai dari lingkungan terdekat kita, patuhilah protokol kesehatan yang ada yakni memakai masker, cuci tangan, menjaga jarak dan juga menghindari kerumunan," kata Gatot.
      <br> <br>Hingga saat ini masker kain GEMAS telah dibagikan langsung oleh relawan di lapangan, dan berbagai NGO, TNI AD, Komunita di antaranya adalah Anak Indonesia Bersatu, Dompet Dhuafa, Kodim Jakarta Barat, GMI Jakarta, Komunitas Slankers, Asensi, Komunitas Spedox Surabaya, Korlantas Polri, Polda Metro Jaya, Kerabat KOTAK Indonesia, Bali Beachwalk, fX Sudirman, Park23, Plaza Indonesia, Pemprov Jawa Timur, Pemprov Banten dan masih banyak lainnya.
      <br> <br>Total Masker yang telah didistribusikan lebih dari 3.5 juta ke seluruh Indonesia menggunakan Ekspedisi SiCepat Ekspres secara gratis. Masker merah putih dari GEMAS ini dipakai oleh banyak artis Tanah Air antara lain, Raffi Ahmad, Maia Estianty, Cinta Laura, Baim Wong, Ari Lasso, Prilly Latuconsina; dan Kaesang Pangarep. Para influencer ikut mengkampanyekan pentingnya memakai masker kain terutama disaat pandemi COVID-19. 
      <br> <br>
      <br> <br>
      <div id="comments">
        <h2>Comments</h2>
        <ul>
          <li>
            <article>
              <header>
                <figure class="avatar"><img src="../images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">A Name</a>
                </address>
                <time datetime="2045-04-06T08:15+00:00">Friday, 6<sup>th</sup> April 2045 @08:15:00</time>
              </header>
              <div class="comcont">
                <p>This is an example of a comment made on a post. You can either edit the comment, delete the comment or reply to the comment. Use this as a place to respond to the post or to share what you are thinking.</p>
              </div>
            </article>
          </li>
          <li>
            <article>
              <header>
                <figure class="avatar"><img src="../images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">A Name</a>
                </address>
                <time datetime="2045-04-06T08:15+00:00">Friday, 6<sup>th</sup> April 2045 @08:15:00</time>
              </header>
              <div class="comcont">
                <p>This is an example of a comment made on a post. You can either edit the comment, delete the comment or reply to the comment. Use this as a place to respond to the post or to share what you are thinking.</p>
              </div>
            </article>
          </li>
          <li>
            <article>
              <header>
                <figure class="avatar"><img src="../images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">A Name</a>
                </address>
                <time datetime="2045-04-06T08:15+00:00">Friday, 6<sup>th</sup> April 2045 @08:15:00</time>
              </header>
              <div class="comcont">
                <p>This is an example of a comment made on a post. You can either edit the comment, delete the comment or reply to the comment. Use this as a place to respond to the post or to share what you are thinking.</p>
              </div>
            </article>
          </li>
        </ul>
        <h2>Write A Comment</h2>
        <form action="#" method="post">
          <div class="one_third first">
            <label for="name">Name <span>*</span></label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
          <div class="one_third">
            <label for="email">Mail <span>*</span></label>
            <input type="email" name="email" id="email" value="" size="22" required>
          </div>
          <div class="one_third">
            <label for="url">Website</label>
            <input type="url" name="url" id="url" value="" size="22">
          </div>
          <div class="block clear">
            <label for="comment">Your Comment</label>
            <textarea name="comment" id="comment" cols="25" rows="10"></textarea>
          </div>
          <div>
            <input type="submit" name="submit" value="Submit Form">
            &nbsp;
            <input type="reset" name="reset" value="Reset Form">
          </div>
        </form>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
@stop