@extends('admin.template')
@section('content')
<br><br><br><br>
<!-- page content -->
    <div class="right_col" role="main">
    	@if(session('sukses'))
			<div class="alert alert-success" role='alert'>
				Data Berhasil Di tambahkan
			</div>	
		@endif
    	<a href="/obat"><h2>Data obat</h2></a>
    	</br>
	    <form action="/obat/cari" method="GET" class="t-header-search-box">
	      <div class="form-group mx-sm-3 mb-2">
	        <label for="cari" class="sr-only">Cari Daftar Obat</label>
	        <input type="text" name="cari" placeholder="Cari Daftar obat .." value="{{ old('cari') }}">
	        <button type="submit" class="btn btn-primary mb-2"> CARI </button> 
	      </div>
	    </form>
    	</br>   
	    <table class="table table-striped table-hover table-bordered">
	    	<thead class="thead-dark">
	        	<tr> 
	            	<th>Nama obat</th>
	            	<th>Harga</th>
	            	<th>Stok</th>
	            	<th>Keterangan</th>
	            	<th>Gambar</th>
	            	<th>Actions</th>
	            </tr> 
	        </thead> 
            @foreach($obat as $o)
	            <tr>
	            	<td>{{ $o->name }}</td>
	            	<td>{{ $o->harga }}</td>
	            	<td>{{ $o->stok }}</td>
	            	<td>{{ $o->keterangan }}</td>
	            	<td><img width="150px" src="{{ url('data_file/'.$o->file) }}"></td>
	              	<td>
	              		<a class="btn btn-warning btn-sm" href="/obat/edit/{{ $o->id }}"> Edit</a>
	              		<a class="btn btn-danger btn-sm" href="/obat/destroy/{{ $o->id }}"> Hapus</a>
	              	</td>   
	            </tr>
            @endforeach
        </table>
        	<br>
          <!-- Button trigger modal -->
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
		  Tambah Daftar Obat Baru
		</button>

		<!-- Modal -->
		<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
		  <div class="modal-dialog">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title" id="exampleModalLabel">Tambah Obat</h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
		      <div class="modal-body">
		        	<form action="obat/create" method="POST" enctype="multipart/form-data">
		        		{{@csrf_field()}}
					  	<div class="form-group">
					    	<label for="name">Nama Obat</label>
					    	<input type="text" name="name" class="form-control" id="name">
					  	</div>
					  	<div class="form-group">
					    	<label for="harga">Harga Obat</label>
					   		<input type="text" name="harga" class="form-control" id="harga">
					  	</div>
					  	<div class="form-group">
					  	<label for="stok">   Pilih Stok   </label>
					  	<select name="stok" class="form-control" id="stok">
							<option value="Tersedia">   Tersedia   </option>
                			<option value="Tidak Tersedia">Tidak Tersedia</option>
						</select>
						<br><br>
						</div>
						<div class="form-group">
					    	<label for="keterangan">Keterangan</label>
					   		<input type="text" name="keterangan" class="form-control" id="keterangan">
					  	</div>
						<div class="form-group">
							<lable for="file" class=" col-form-label">Gambar Obat</lable><br>
							<div class="form-group">	
							<input type="file" name="file"><br>
							</div>
						</div>
		      			<div class="modal-footer">
		        			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		        			<button type="submit" class="btn btn-primary">Tambah</button>
		      			</div>
		      		</form>
		      	</div>
			</div>
		  </div>
		</div>
  </div>
 </div>
@stop    