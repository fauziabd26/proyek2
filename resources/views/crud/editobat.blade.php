<div class="right_col" role="main">
    <h2>Edit Data Obat</h2>
    @if(session('sukses'))
		<div class="alert alert-success" role='alert'>
			Data Berhasil Di Ubah
		</div>	
	@endif
	<div class="row">
		<div class="col-lg-12">
			<div class="card-body">
				<form action="/obat/update/{{ $obat->id }}" method="POST">
					{{ csrf_field() }}
                    {{ method_field('PUT') }}
				  	<div class="form-group">
				    	<label for="name">Nama Obat</label>
				    	<input type="text" name="name" class="form-control" id="name" value="{{$obat->name}}">
				  	</div>
					<div class="form-group">
					    <label for="harga">Harga Obat</label>
					   	<input type="text" name="harga" class="form-control" id="harga" value="{{$obat->harga}}">
					</div>
					<div class="form-group">
						<label for="stok">   Pilih Stok   </label>
						<select name="stok" class="form-control" id="stok">
							<option value="Tersedia" @if($obat->stok == 'Tersedia') selected @endif>   Tersedia   </option>
                			<option value="Tidak Tersedia" @if($obat->stok == 'Tidak Tersedia') selected @endif>Tidak Tersedia</option>
						</select>
						<br><br>
					</div>
					<div class="form-group">
					    <label for="keterangan">Keterangan</label>
					   	<input type="text" name="keterangan" class="form-control" id="keterangan" value="{{$obat->keterangan}}">
					</div>
					<div class="form-group">
						<div class="form-group">	
							<input type="file" name="file" id="file"><br>
						</div>
					</div>
					<div class="form-group">
						<lable for="file" class=" col-form-label">Gambar Obat</lable>
						<img width="150px" src="{{ url('data_file/'.$obat->file) }}">
					</div>
		      		<button type="submit" class="btn btn-primary">Save Changes</button>
		      	</form>
		    </div>
		</div>
	</div>
</div>
</br>