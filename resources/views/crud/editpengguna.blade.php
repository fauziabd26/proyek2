<div class="right_col" role="main">
    <h2>Edit Data Pengguna</h2>
    @if(session('sukses'))
		<div class="alert alert-success" role='alert'>
			{{session('sukses')}}
		</div>	
	@endif
	<div class="row">
		<div class="col-lg-12">
			<div class="card-body">
				<form action="/pengguna/update/{{ $pengguna->id }}" method="POST">
					{{ csrf_field() }}
                    {{ method_field('PUT') }}
				  	<div class="form-group">
				    	<label for="name">Nama</label>
				    	<input type="text" name="name" class="form-control" id="name" value="{{$pengguna->name}}">
				  	</div>
					<div class="form-group">
					    <label for="no">Nomor Telepon</label>
					   	<input type="text" name="no" class="form-control" id="no" value="{{$pengguna->no}}">
					</div>
					<div class="form-group">
					    <label for="email">Email</label>
					   	<input type="email" name="email" class="form-control" id="email" value="{{$pengguna->email}}">
					</div>
		      		<button type="submit" class="btn btn-primary">Save Changes</button>
		      	</form>
		    </div>
		</div>
	</div>
</div>
</br>