@extends('template')
@section('tittle','Informasi Kesehatan')
@section('content')
<br>

<div class="wrapper row3">
  <main class="hoc container clear"> 
    <div class="content"> 
      <h6> INFORMASI KESEHATAN </h6>
      <div class="row">
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">PENANGANAN COVID-19</h4>
            <div class="card-body">
              <p class="card-text">ANDA MERASA TIDAK SEHAT <br> 1. Jika Anda merasa tidak sehat dengan kriteria:
              <br> a. Demam 38 derajat Celcius, dan
              <br> b. Batuk/pilek istirahatlah yang cukup di rumah dan bila perlu minum Bila keluhan berlanjut,...</p>
            </div>
            <div class="card-footer">
              <a href="protokol" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>
        <div class="col-lg-4 mb-4">
          <div class="card h-100">
            <h4 class="card-header">GERAKAN 3M</h4>
            <div class="card-body">
              <p class="card-text">Masker terbukti sebagai salah satu cara yang paling efektif untuk mencegah penularan COVID-19. Menyadari akan hal tersebut, para relawan yang tergabung dalam Gerakan Memakai Masker Gratis (GEMAS) membagikan 50 ribu masker....</p>
            </div>
            <div class="card-footer">
              <a href="#" class="btn btn-primary">Read More</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
</div>
@stop