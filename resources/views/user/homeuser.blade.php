@extends('user.template')
@section('tittle','Dashboard')
@section('content')
<br><br><br><br>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Selamat Datang</div>
                <div class="card-body">
                  @if (session('status'))
                    <div class="alert alert-success" role="alert">
                      {{ session('status') }}
                    </div>
                  @endif
                  {{ Auth::user()->name }}
                </div>
            </div>
            <br>
            <h1>Apa yang ingin anda Lakukan?</h1>
        </div>
    </div>
    <br><br>
    <div class="row">
      <div class="col-lg-4 mb-4">
        <div class="card h-100">
          <h4 class="card-header">Ambil Nomor Antrian</h4>
          <div class="card-body">
            <p class="card-text">Jika anda ingin berobat maka harus mengambil nomor antrian terebih dahulu. <br><br><br><br><br></p>
          </div>
          <div class="card-footer">
            <a href="/homeuser" class="btn btn-primary">Ambil No. Antrian</a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100">
          <h4 class="card-header">Pesan Obat</h4>
          <div class="card-body">
            <p class="card-text">Jika ingin pesan obat anda bisa memesan langsung disini. <br><br><br><br><br></p>
          </div>
          <div class="card-footer">
            <a href="/showobat" class="btn btn-primary">Pesan Obat</a>
          </div>
        </div>
      </div>
      <div class="col-lg-4 mb-4">
        <div class="card h-100">
          <h4 class="card-header">Konsultasi</h4>
          <div class="card-body">
            <p class="card-text">Konsultasi bisa lewat chat. <br><br><br><br><br></p>
          </div>
          <div class="card-footer">
            <a href="/homeuser" class="btn btn-primary">Konsultasi sekarang</a>
          </div>
        </div>
      </div>
    </div>
    <br><br><br>
</div>
<br><br>
@endsection