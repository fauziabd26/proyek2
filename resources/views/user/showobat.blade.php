@extends('user.template')
@section('tittle','Obat')
@section('content')
<div class="container">
  <div class="row">  
    <div class="col-md-12 mb-5" role="main">
      <a href="/showobat"><h2>Lihat Data obat</h2></a>
    </div>
    @foreach($obat as $o)
    <div class="col-md-4">
      <div class="card">
        <h4 class="card-header">{{ $o->name }}</h4>
        <div class="card-body">
          <img width="200px" src="{{ url('data_file/'.$o->file) }}"> 
          <p class="card-text"><strong>Harga : </strong>Rp.{{ $o->harga }}<br><strong>Stok : </strong>{{ $o->stok }} <br> <strong>Keterangan : </strong>{{ $o->keterangan }}</p>
        </div>
        <div class="card-footer">
          <a href="/pemesanan" class="btn btn-primary" class="fa fa-shopping-cart">Pesan Sekarang</a>
        </div>
      </div>
      <br><br>
    </div>
    @endforeach
  </div>
</div>
@endsection
