<!DOCTYPE html>
<html>
<head>
<title>Login</title>
<!-- For-Mobile-Apps -->
<meta name="viewport" content="width=device-width, initial-scale=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="User Icon Login Form Widget Responsive, Login Form Web Template, Flat Pricing Tables, Flat Drop-Downs, Sign-Up Web Templates, Flat Web Templates, Login Sign-up Responsive Web Template, Smartphone Compatible Web Template, Free Web Designs for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //For-Mobile-Apps -->
<link rel="stylesheet" href="signin/css/style.css" type="text/css" media="all" />
</head>
<body>
<div class="container">
<h1>LOGIN FORM</h1>
     <div class="contact-form">
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</div>
</body>
</html>