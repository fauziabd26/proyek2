@extends('template')
@section('tittle','protokol')
@section('content')
<div class="wrapper row3">
  <main class="hoc container clear"> 
    <!-- main body -->
    <!-- ################################################################################################ -->
    <div class="content"> 
      <!-- ################################################################################################ -->
      <h6>PROTOKOL KESEHATAN PENANGANAN COVID-19</h6>
      <img class="imgl borderedbox inspace-5" src="../images/protokol.jpeg" alt="">
      <p>ANDA MERASA TIDAK SEHAT <br> 1. Jika Anda merasa tidak sehat dengan kriteria:
      <br> a. Demam 38 derajat Celcius, dan
      <br> b. Batuk/pilekistirahatlah yang cukup di rumah dan bila perlu minum Bila keluhan berlanjut, ataudisertai dengan kesulitan bernafas (sesak atau nafas cepat), segera berobat kefasilitas pelayanan kesehatan (fasyankes)
      Pada saat berobat ke fasyankes, <br> Anda harus lakukan tindakan berikut:
      <br>a. Gunakan masker
      <br>b. Apabila tidak memiliki masker, ikuti etika batuk/bersin yang benar dengancara menutup mulut dan hidung dengan tisu atau punggung lengan
      <br> c. Usahakan tidak menggunakan transportasi massal
      <br><br> 2. Tenaga kesehatan (nakes) di fasyankes akan melakukan screening suspect COVID-19:
      <br> a. Jika memenuhi kriteria suspect COVID-19, maka Anda akan dirujuk kesalah satu rumah sakit (RS) rujukan yang siap untuk penanganan COVID19.
      <br> b. Jika tidak memenuhi kriteria suspect COVID-19, maka Anda akan dirawatinap atau rawat jalan tergantung diagnosa dan keputusan dokterfasyankes.

      <br> 3. Jika anda memenuhi kriteria Suspect COVID-19 akan diantar ke RS rujukanmenggunakan ambulan fasyankes didampingi oleh nakes yang menggunakanalat pelindung diri (APD).

      <br> 4. Di RS rujukan, akan dilakukan pengambilan spesimen untuk pemeriksaanlaboratorium dan dirawat di ruang isolasi.

      <br>5. Spesimen akan dikirim ke Badan Penelitian dan Pengembangan Kesehatan(Balitbangkes) di Jakarta. Hasil pemeriksaan pertama akan keluar dalam 24 jamsetelah spesimen diterima.
      <br><br> a. Jika hasilnya positif,
      <br>i. maka Anda akan dinyatakan sebagai penderita COVID-19.
      <br>ii. Sampel akan diambil setiap hari      
      <br>iii. Anda akan dikeluarkan dari ruang isolasi jika pemeriksaan sampel
    2 (dua) kali berturut-turut hasilnya negatif
    <br><br> b. Jika hasilnya negatif, Anda akan dirawat sesuai dengan penyebabpenyakit.
      <br>JIKA ANDA SEHAT, namun:
      <br>1. Ada riwayat perjalanan 14 hari yang lalu ke negara terjangkit COVID-19, ATAU
      <br>2. Merasa pernah kontak dengan penderita COVID-19,hubungi Hotline Center Corona untuk mendapat petunjuk lebih lanjut di nomorberikut: 119 ext 9.
      </p>
      <div id="comments">
        <h2>Comments</h2>
        <ul>
          <li>
            <article>
              <header>
                <figure class="avatar"><img src="../images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">A Name</a>
                </address>
                <time datetime="2045-04-06T08:15+00:00">Friday, 6<sup>th</sup> April 2045 @08:15:00</time>
              </header>
              <div class="comcont">
                <p>This is an example of a comment made on a post. You can either edit the comment, delete the comment or reply to the comment. Use this as a place to respond to the post or to share what you are thinking.</p>
              </div>
            </article>
          </li>
          <li>
            <article>
              <header>
                <figure class="avatar"><img src="../images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">A Name</a>
                </address>
                <time datetime="2045-04-06T08:15+00:00">Friday, 6<sup>th</sup> April 2045 @08:15:00</time>
              </header>
              <div class="comcont">
                <p>This is an example of a comment made on a post. You can either edit the comment, delete the comment or reply to the comment. Use this as a place to respond to the post or to share what you are thinking.</p>
              </div>
            </article>
          </li>
          <li>
            <article>
              <header>
                <figure class="avatar"><img src="../images/demo/avatar.png" alt=""></figure>
                <address>
                By <a href="#">A Name</a>
                </address>
                <time datetime="2045-04-06T08:15+00:00">Friday, 6<sup>th</sup> April 2045 @08:15:00</time>
              </header>
              <div class="comcont">
                <p>This is an example of a comment made on a post. You can either edit the comment, delete the comment or reply to the comment. Use this as a place to respond to the post or to share what you are thinking.</p>
              </div>
            </article>
          </li>
        </ul>
        <h2>Write A Comment</h2>
        <form action="#" method="post">
          <div class="one_third first">
            <label for="name">Name <span>*</span></label>
            <input type="text" name="name" id="name" value="" size="22" required>
          </div>
          <div class="one_third">
            <label for="email">Mail <span>*</span></label>
            <input type="email" name="email" id="email" value="" size="22" required>
          </div>
          <div class="one_third">
            <label for="url">Website</label>
            <input type="url" name="url" id="url" value="" size="22">
          </div>
          <div class="block clear">
            <label for="comment">Your Comment</label>
            <textarea name="comment" id="comment" cols="25" rows="10"></textarea>
          </div>
          <div>
            <input type="submit" name="submit" value="Submit Form">
            &nbsp;
            <input type="reset" name="reset" value="Reset Form">
          </div>
        </form>
      </div>
      <!-- ################################################################################################ -->
    </div>
    <!-- ################################################################################################ -->
    <!-- / main body -->
    <div class="clear"></div>
  </main>
</div>
@stop