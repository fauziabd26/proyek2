@extends('template')
@section('tittle','Klinik Kesehatan')
@section('content')
<br>
<br>

  <header>
    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
        <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
        <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!-- Slide One - Set the background image for this slide in the line below -->
        <div class="carousel-item active" style="background-image: url('../home/images/tempat.png')">
          <div class="carousel-caption d-none d-md-block">
            <h3>First Slide</h3>
            <p>This is a description for the first slide.</p>
          </div>
        </div>
        <!-- Slide Two - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h3>Second Slide</h3>
            <p>This is a description for the second slide.</p>
          </div>
        </div>
        <!-- Slide Three - Set the background image for this slide in the line below -->
        <div class="carousel-item" style="background-image: url('http://placehold.it/1900x1080')">
          <div class="carousel-caption d-none d-md-block">
            <h3>Third Slide</h3>
            <p>This is a description for the third slide.</p>
          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
  </header>

  <!-- Page Content -->
  <div class="container">

    <h1 class="my-4">Welcome to Klinik Kesehatan dr. Jamil</h1>

    <!-- Marketing Icons Section -->
    <div class="row">
      <div class="col-lg-4 mb-4">
        <div class="card h-100">
          <h4 class="card-header">Ambil Nomor Antrian</h4>
          <div class="card-body">
            <p class="card-text">Jika anda ingin berobat maka harus mengambil nomor antrian terebih dahulu. <br><br><br><br><br></p>
          </div>
          <div class="card-footer">
            <a href="/homeuser" class="btn btn-primary">Ambil No. Antrian</a>
          </div>
        </div>
      </div>
    </div>
    <br><br><br><br><br>
    <!-- /.row -->

    <!-- Features Section -->
    <div class="row">
      <div class="col-lg-6">
        <h2>Tentang Klinik</h2>
        <p>Kelebihan berobat di klinik:</p>
        <ul>
          <li>Mudah</li>
          <li>Dekat</li>
          <li>Cepat penanganannya</li>
        </ul>
        <p><strong>Klinik dan Praktek umum dr.jamil yang berdiri sejak 00-00-0000 <br> Alamat Jln. Suren no.08 (Perumahan Griya Asri 1), Pekandangan kec. Indramayu Kab. Indramayu  45216</strong></p>
      </div>
      <div class="col-lg-6">
        <img class="img-fluid rounded" src="../home/images/tempat.png" alt="">
      </div>
    </div>
    <!-- /.row -->

    <hr>

  </div>
  <!-- /.container -->
@stop