<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>@yield('tittle')</title>

  <!-- Bootstrap core CSS -->
  <link href="home/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="home/css/modern-business.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
      <a class="navbar-brand" href="/">Klinik Kesehatan</a>
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="/">Home</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownPortfolio" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              About
            </a>
            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownPortfolio">
              <a class="dropdown-item" href="/galeri">Gallery</a>
              <a class="dropdown-item" href="/klinik">Klinik</a>
              <a class="dropdown-item" href="/contact">Contact Us</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/showobat">Pemesanan Obat</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/informasi">Informasi Kesehatan</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#">Konsultasi</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="/login">Login/Register</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  @show
  @yield('content')

  <!-- Footer -->
  <footer id="footer" class="py-5 bg-dark"> 
    <div class="one_third first text-white">
      <h6 class="heading">Contact Us</h6>
      <ul class="nospace btmspace-30 linklist contact">
        <li><i class="fa fa-map-marker"></i>Indramayu, Jawa Barat, 12345</li>
        <li><i class="fa fa-phone"></i> +62 812345678</li>
        <li><i class="fa fa-fax"></i> (021) 456 7890</li>
        <li><i class="fa fa-envelope-o"></i> fauzi.aaf@gmail.com</li>
      </ul>
      <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Kelompok Proyek 2 2020</p>
    </div>
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="home/vendor/jquery/jquery.min.js"></script>
  <script src="home/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

</body>

</html>
