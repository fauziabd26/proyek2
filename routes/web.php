<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/pengguna', 'PenggunaController@index'); 
Route::post('/pengguna/create', 'PenggunaController@create'); 
Route::get('/pengguna/edit/{id}','PenggunaController@edit');
Route::put('/pengguna/update/{id}','PenggunaController@update');
Route::get('/pengguna/destroy/{id}','PenggunaController@destroy');
  
//Route Login,Register
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout')->name('logout');
Route::get('register', 'AuthController@showFormRegister')->name('register');
Route::post('registerpost', 'AuthController@register');
Route::get('loginadmin', 'AdminAuthController@showFormLogin')->name('loginadmin');
Route::get('/homeadmin', 'HomeController@admin')->name('homeadmin')->middleware('is_admin');

Route::group(['middleware' => 'auth'], function () {   
  //Route Crud
  Route::get('/obat','ObatController@index');
  Route::post('/obat/create','ObatController@create');
  Route::post('/obat/store','ObatController@store');
  Route::get('/obat/cari','ObatController@cari');
  Route::get('/obat/edit/{id}','ObatController@edit');
  Route::put('/obat/update/{id}','ObatController@update');
  Route::get('/obat/destroy/{id}','ObatController@destroy');
  Route::get('/showobat','ObatController@show');
  Route::get('homeuser', 'HomeController@index')->name('homeuser');
  Route::get('profile', 'ProfileController@edit')->name('profile.edit');
  Route::patch('profile', 'ProfileController@update')->name('profile.update');
  Route::get('password', 'PasswordController@edit')->name('user.password.edit');
  Route::patch('password', 'PasswordController@update')->name('user.password.update');
  Route::get('/showobat','HomeController@obat')->name('obat');

});

//Route Menu
Route::get('/', 'MenuController@index')->name('home');
Route::get('galeri','MenuController@galeri')->name('galeri');
Route::get('klinik','MenuController@klinik')->name('klinik');
Route::get('informasi','MenuController@informasi')->name('informasi');
Route::get('protokol','MenuController@protokol')->name('protokol');
Route::get('germas','MenuController@germas')->name('germas');
Route::get('/register','MenuController@register')->name('register');
Route::get('contact','ContactController@contact')->name('contact');
Route::post('contactpost','ContactController@contactpost');
